# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 14:01:46 2020

@author: Mads-
"""

import numpy as np
from sklearn import model_selection
from sklearn import preprocessing
from scipy import stats
from random import randint
#Raw data, no preprocessing 100x300
x = np.genfromtxt("/Users/Johannesreiche/Library/Mobile Documents/com~apple~CloudDocs/DTU/KID/Januar 2020/statistical-evaluation-of-AI/proj1/df_exp.csv")
y = x[:,x.shape[1]-1]
x = x[:,0:300]


#Transformed data. Curvelength, span of x,y,z-direction
#x2 = np.genfromtxt("C:/Users/Mads-/Documents/Universitet/3. Semester/02445 Statistical evaluation of intelligent system/Source code/statistical-evaluation-of-AI/proj1/william/experiment_effect.csv")
#x2 = x2[:,0:10]



#x2 = x2[698:798,]

#x = np.hstack((x,x2))

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier


#x = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/x9.csv", delimiter=' ')
#y = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/y9.csv", delimiter=' ')

#stadardizing x
x = preprocessing.scale(x)
#x = np.round(x,4)

#100 for leave one out.
K = 10
CV = model_selection.KFold(n_splits=K,shuffle=True)
#random_state = 12

svm_predict = np.array([])
LDA_predict = np.array([])
KNN_predict = np.array([])
GNB_predict = np.array([])
clf_predict = np.array([])
lreg_predict = np.array([])
DecisionTree_predict = np.array([])
RF_predict = np.array([])
LR_predict = np.zeros(50).reshape(1,50)

opt_lambda = np.array([])

y_true = np.array([])

c = 0
measurements_svm = np.array([])
measurements_dct = np.array([])
measurements_nnet = np.array([])
measurements_GNB = np.array([])
measurements_RF = np.array([])




#x_test = np.zeros(10)
#for j in range(10):
#    x_test[j] = randint(0,9)+((10)*j)
#x_test = x_test.astype('int64')
#x_train = np.arange(0,100)
#x_train = x_train.astype('int64')
#for i in range(10):
#    x_train = x_train[x_train != x_test[i]]
#print(x_test)
#print("\n", x_train)
    

val_lg = np.array([])
val_nn = np.array([])
val_sv = np.array([])


# store predictions.
yhat = []
y_true = []

for i in range(100):
    test_index = np.zeros(10)
    for j in range(10):
        test_index[j] = randint(0,9)+((10)*j)
    test_index =test_index.astype('int64')
    train_index = np.arange(0,100)
    train_index  = train_index .astype('int64')
    for i in range(10):
        train_index  = train_index[train_index  != test_index[i]]
    x_train = x[train_index]
    x_test = x[test_index]
    
    y_train = y[train_index]
    y_test = y[test_index]
    y_true = np.append(y_true,y_test)
    
    #support vector machine
    m_svm = SVC(gamma='auto', kernel = "linear")
    m_svm.fit(x_train,y_train)
    svm_predict = np.append(svm_predict,m_svm.predict(x_test))
    
    #m_DecisionTree = DecisionTreeClassifier()
    #m_DecisionTree.fit(x_train,y_train)
    #DecisionTree_predict = np.append(DecisionTree_predict,m_DecisionTree.predict(x_test))
        
        
    clf = MLPClassifier(activation ='relu', solver='adam', alpha=1e-5, hidden_layer_sizes=(200,200, 10), max_iter = 300, momentum = 0.9)
    clf.fit(x_train,y_train)
    clf_predict = np.append(clf_predict,clf.predict(x_test))
        
    #m_gaus = GaussianNB()
    #m_gaus.fit(x_train,y_train)
    #GNB_predict = np.append(GNB_predict,m_gaus.predict(x_test))
        
    #ranFor = RandomForestClassifier(n_estimators=100)
    #ranFor.fit(x_train,y_train)
    #RF_predict = np.append(RF_predict,ranFor.predict(x_test))
    
    lreg = LogisticRegression(penalty ="l2",solver = "saga",multi_class = "auto",max_iter= 500)
    lreg.fit(x_train, y_train)
    lreg_predict = np.append(lreg_predict,lreg.predict(x_test))
        
    #print("svm_acc:", np.mean(y_true == svm_predict))
    #print("Dec:", np.mean(y_true == DecisionTree_predict))
    #print("MLP:", np.mean(y_true==clf_predict))
    #print("GNB:", np.mean(y_true==GNB_predict))
    #print("RandForest:", np.mean(y_true==RF_predict))
    val_lg = np.append(val_lg,np.mean(y_true==lreg_predict))
    val_nn = np.append(val_nn,np.mean(y_true==clf_predict))
    val_sv = np.append(val_sv,np.mean(y_true==svm_predict))

    
    #for i in range(10):
        #print("svm_acc:", np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))
     #   measurements_svm = np.append(measurements_svm,np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))
        #print("Dec:", np.mean(y_true[i*10:i*10+10] == DecisionTree_predict[i*10:i*10+10]))
       # measurements_dct = np.append(measurements_dct,np.mean(y_true[i*10:i*10+10] == DecisionTree_predict[i*10:i*10+10]))
        #print("MLP:", np.mean(y_true[i*10:i*10+10]==clf_predict[i*10:i*10+10]))
        #measurements_nnet = np.append(measurements_nnet,np.mean(y_true[i*10:i*10+10]==clf_predict[i*10:i*10+10]))
        #measurements_GNB = np.append(measurements_GNB,np.mean(y_true[i*10:i*10+10]==GNB_predict[i*10:i*10+10]))
        #measurements_RF = np.append(measurements_RF,np.mean(y_true[i*10:i*10+10]==RF_predict[i*10:i*10+10]))

def confint(vector, interval):
  # Standard deviation of sample
  vec_sd = np.std(vector)
  # Sample size
  n = len(vector)
  # Mean of sample
  vec_mean = np.mean(vector)
  # Error according to t distribution
  error = 1.984217 * vec_sd / np.sqrt(n)
  # Confidence interval as a vector
  result = np.array([vec_mean - error,vec_mean + error])
  return(result)
  
print("svm_acc:", np.mean(y_true == svm_predict))
#print("Dec:", np.mean(y_true == DecisionTree_predict))
print("MLP:", np.mean(y_true==clf_predict))
#print("GNB:", np.mean(y_true==GNB_predict))
print("LG:", np.mean(y_true==lreg_predict))

print("CI nn: ", confint(val_nn,0.95))
print("CI lg: ", confint(val_lg,0.95))
print("CI sv: ", confint(val_sv,0.95))

#print(np.std(y_true == svm_predict),np.std(y_true == DecisionTree_predict),np.std(y_true==clf_predict),np.std(y_true==GNB_predict))

