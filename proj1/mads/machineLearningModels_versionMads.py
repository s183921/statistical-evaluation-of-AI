# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 14:01:46 2020

@author: Mads-
"""

import numpy as np
from sklearn import model_selection
from sklearn import preprocessing
from scipy import stats
#Raw data, no preprocessing 100x300
x = np.genfromtxt("C:/Users/Mads-/Documents/Universitet/3. Semester/02445 Statistical evaluation of intelligent system/Source code/statistical-evaluation-of-AI/proj1/df_exp.csv")
y = x[:,x.shape[1]-1]
x = x[:,0:300]


#Transformed data. Curvelength, span of x,y,z-direction
x2 = np.genfromtxt("C:/Users/Mads-/Documents/Universitet/3. Semester/02445 Statistical evaluation of intelligent system/Source code/statistical-evaluation-of-AI/proj1/william/experiment_effect.csv")
x2 = x2[:,0:10]



x2 = x2[698:798,]

x = np.hstack((x,x2))

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier


#x = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/x9.csv", delimiter=' ')
#y = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/y9.csv", delimiter=' ')

#stadardizing x
x = preprocessing.scale(x)
#x = np.round(x,4)

#100 for leave one out.
K = 100
CV = model_selection.KFold(n_splits=K,shuffle=True)
#random_state = 12

svm_predict = np.array([])
LDA_predict = np.array([])
KNN_predict = np.array([])
GNB_predict = np.array([])
clf_predict = np.array([])
DecisionTree_predict = np.array([])
RF_predict = np.array([])
LR_predict = np.zeros(50).reshape(1,50)

opt_lambda = np.array([])

y_true = np.array([])

c = 0
measurements_svm = np.array([])
measurements_dct = np.array([])
measurements_nnet = np.array([])
measurements_GNB = np.array([])
measurements_RF = np.array([])

for i in range(10):
    for (train_index, test_index) in CV.split(x,y):
        x_train = x[train_index]
        x_test = x[test_index]
        
        y_train = y[train_index]
        y_test = y[test_index]
        y_true = np.append(y_true,y_test)
        
        #support vector machine
        m_svm = SVC(gamma = "auto",kernel = "linear")
        m_svm.fit(x_train,y_train)
        svm_predict = np.append(svm_predict,m_svm.predict(x_test))
        
        m_DecisionTree = DecisionTreeClassifier()
        m_DecisionTree.fit(x_train,y_train)
        DecisionTree_predict = np.append(DecisionTree_predict,m_DecisionTree.predict(x_test))
        
        
        clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(20,20,20, 10), random_state=1)
        clf.fit(x_train,y_train)
        clf_predict = np.append(clf_predict,clf.predict(x_test))
        
        m_gaus = GaussianNB()
        m_gaus.fit(x_train,y_train)
        GNB_predict = np.append(GNB_predict,m_gaus.predict(x_test))
        
        ranFor = RandomForestClassifier()
        ranFor.fit(x_train,y_train,criterion=”gini”,n_estimators=100)
        RF_predict = np.append(RF_predict,ranFor.predict(x_test))
        
    print("svm_acc:", np.mean(y_true == svm_predict))
    print("Dec:", np.mean(y_true == DecisionTree_predict))
    print("MLP:", np.mean(y_true==clf_predict))
    print("GNB:", np.mean(y_true==GNB_predict))
    print("RandForest:", np.mean(y_true==RF_predict))

    
    for i in range(10):
        #print("svm_acc:", np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))
        measurements_svm = np.append(measurements_svm,np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))
        #print("Dec:", np.mean(y_true[i*10:i*10+10] == DecisionTree_predict[i*10:i*10+10]))
        measurements_dct = np.append(measurements_dct,np.mean(y_true[i*10:i*10+10] == DecisionTree_predict[i*10:i*10+10]))
        #print("MLP:", np.mean(y_true[i*10:i*10+10]==clf_predict[i*10:i*10+10]))
        measurements_nnet = np.append(measurements_nnet,np.mean(y_true[i*10:i*10+10]==clf_predict[i*10:i*10+10]))
        measurements_GNB = np.append(measurements_GNB,np.mean(y_true[i*10:i*10+10]==GNB_predict[i*10:i*10+10]))
        measurements_RF = np.append(measurements_RF,np.mean(y_true[i*10:i*10+10]==RF_predict[i*10:i*10+10]))

print("svm_acc:", np.mean(y_true == svm_predict))
print("Dec:", np.mean(y_true == DecisionTree_predict))
print("MLP:", np.mean(y_true==clf_predict))
print("GNB:", np.mean(y_true==GNB_predict))
print("RF:", np.mean(y_true==GNB_predict))

print(np.std(y_true == svm_predict),np.std(y_true == DecisionTree_predict),np.std(y_true==clf_predict),np.std(y_true==GNB_predict))

