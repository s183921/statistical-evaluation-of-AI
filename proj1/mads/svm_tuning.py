# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 13:43:08 2020

@author: Mads-
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 14:01:46 2020

@author: Mads-
"""

import numpy as np
from sklearn import model_selection
from sklearn import preprocessing
from scipy import stats
from sklearn.svm import SVC
import random
random.seed(2)
#Raw data, no preprocessing 100x300
x = np.genfromtxt("C:/Users/Mads-/Documents/Universitet/3. Semester/02445 Statistical evaluation of intelligent system/Source code/statistical-evaluation-of-AI/proj1/df_exp.csv")
y = x[:,300]
x = x[:,0:300]


#Transformed data. Curvelength, span of x,y,z-direction
x2 = np.genfromtxt("C:/Users/Mads-/Documents/Universitet/3. Semester/02445 Statistical evaluation of intelligent system/Source code/statistical-evaluation-of-AI/proj1/william/experiment_effect.csv")
#x2 = x[~np.isnan(x).any(axis=1)]
#y2 = x[:,4]
x2 = x2[:,0:4]

    
x2 = x2[700:800,]

#x = np.hstack((x,x2))



#x = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/x9.csv", delimiter=' ')
#y = np.genfromtxt("C:/Users/Karlu/Desktop/Proj1/y9.csv", delimiter=' ')

#stadardizing x
x = preprocessing.scale(x)
#x = np.round(x,4)

#100 for leave one out.
K = 100
CV = model_selection.KFold(n_splits=K,shuffle=True)
#random_state = 12

svm_predict = np.array([])


opt_lambda = np.array([])

y_true = np.array([])

c = 0
measurements_svm = np.array([])


for i in range(10):
    for (train_index, test_index) in CV.split(x,y):
        x_train = x[train_index]
        x_test = x[test_index]
        
        y_train = y[train_index]
        y_test = y[test_index]
        y_true = np.append(y_true,y_test)
        
        #support vector machine
        m_svm = SVC(kernel = "linear")
        m_svm.fit(x_train,y_train)
        svm_predict = np.append(svm_predict,m_svm.predict(x_test))
        
        
        print("svm_acc:", np.mean(y_true == svm_predict))
   
    for i in range(10):
        print("svm_acc:", np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))
        measurements_svm = np.append(measurements_svm,np.mean(y_true[i*10:i*10+10] == svm_predict[i*10:i*10+10]))

print("svm_acc:", np.mean(y_true == svm_predict))


 #‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’