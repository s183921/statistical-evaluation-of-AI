#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 09:20:01 2020

@author: Johannesreiche
"""

import numpy as np
import random

def training_index(t_index,l):
    train_index = np.arange(0,100)
    train_index  = train_index .astype('int64')
    for i in range(10):
        train_index  = train_index[train_index  != t_index[l,i]]
    random.shuffle(train_index)
    return train_index