#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 19 12:46:49 2020

@author: Johannesreiche
"""
import numpy as np
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
import random
from mcnemar import *
from train_index import training_index

#Raw data, no preprocessing 100x300
x = np.genfromtxt("/Users/Johannesreiche/Library/Mobile Documents/com~apple~CloudDocs/DTU/KID/Januar 2020/statistical-evaluation-of-AI/proj1/df_exp.csv")
y = x[:,300]
x = x[:,0:300]

#stadardizing x
x = preprocessing.scale(x)


svm_predict = np.array([])
clf_predict = np.array([])
lreg_predict = np.array([])


val_lg = np.array([])
val_nn = np.array([])
val_sv = np.array([])


# store predictions.
yhat = []
y_true = []
y_trueval = np.array([])

for i in range(100):
    test_index = np.zeros( (10, 10) )
    for j in range(10):
        test_index[:,j] = [x for x in range(0+j*10,10+j*10)]
    for k in range(10):
        random.shuffle(test_index[:,k])
    test_index =test_index.astype('int64')
    
    for l in range(10):
        train_index = training_index(test_index,l)
        random.shuffle(test_index[l,:])
        
        x_train = x[train_index]
        x_test = x[test_index[l,:]]
        y_train = y[train_index]
        y_test = y[test_index[l,:]]
        y_trueval = np.append(y_trueval,y_test)
    
            
        dy = []
    
        m_svm = SVC(kernel = "linear")
        m_svm.fit(x_train,y_train)
        svm_predict = np.append(svm_predict,m_svm.predict(x_test))
        dy.append(m_svm.predict(x_test))
            
        clf = MLPClassifier(activation ='relu', solver='adam', alpha=1e-5, hidden_layer_sizes=(200,200, 10), max_iter = 300, momentum = 0.9)
        clf.fit(x_train,y_train)
        clf_predict = np.append(clf_predict,clf.predict(x_test))
        dy.append(clf.predict(x_test))
        
        lreg = LogisticRegression(penalty ="l2",solver = "saga",multi_class = "auto",max_iter= 500)
        lreg.fit(x_train, y_train)
        lreg_predict = np.append(lreg_predict,lreg.predict(x_test))   
        dy.append(lreg.predict(x_test))
        
        
        
        
        dy = np.stack(dy, axis=1)
        yhat.append(dy)
        y_true.append(y_test)
        val_lg = np.append(val_lg,np.mean(y_trueval==lreg_predict))
        val_nn = np.append(val_nn,np.mean(y_trueval==clf_predict))
        val_sv = np.append(val_sv,np.mean(y_trueval==svm_predict))
        i+=1
    

yhat = np.concatenate(yhat)
y_true = np.concatenate(y_true)
yhat[:,0]

yhat2 = np.zeros ( (10000,4) )
yhat2[:,0:3] = yhat
yhat2[:,3] = 1

print("Support Vector Machine acc:", np.mean(val_sv))
print("Neural Network acc:", np.mean(val_nn))
print("Logistic Regression acc:", np.mean(val_lg))
print("CI svm: ", confint(val_sv,0.95)) 
print("CI nn: ", confint(val_nn,0.95))
print("CI lg: ", confint(val_lg,0.95))
 
alpha = 0.05
print("\n svm vs nn")
[thetahat1, CI1, p1] = mcnemar(y_true, yhat2[:,0], yhat2[:,1], alpha=alpha)
print("\n svm vs lg")
[thetahat2, CI2, p2] = mcnemar(y_true, yhat2[:,0], yhat2[:,2], alpha=alpha)
print("\n svm vs base")
[thetahat3, CI3, p3] = mcnemar(y_true, yhat2[:,0], yhat2[:,3], alpha=alpha)
print("\n nn vs lg")
[thetahat4, CI4, p4] = mcnemar(y_true, yhat2[:,1], yhat2[:,2], alpha=alpha)
print("\n nn vs base")
[thetahat5, CI5, p5] = mcnemar(y_true, yhat2[:,1], yhat2[:,3], alpha=alpha)
print("\n lg vs base")
[thetahat6, CI6, p6] = mcnemar(y_true, yhat2[:,2], yhat2[:,3], alpha=alpha)


print("theta = theta_svm-theta_nn point estimate", thetahat1, " CI: ", CI1, "p-value", p1)
print("theta = theta_svm-theta_lg point estimate", thetahat2, " CI: ", CI2, "p-value", p2)
print("theta = theta_svm-theta_base point estimate", thetahat3, " CI: ", CI3, "p-value", p3)
print("theta = theta_nn-theta_lg point estimate", thetahat4, " CI: ", CI4, "p-value", p4)
print("theta = theta_nn-theta_base point estimate", thetahat5, " CI: ", CI5, "p-value", p6)
print("theta = theta_lg-theta_base point estimate", thetahat6, " CI: ", CI6, "p-value", p6)
