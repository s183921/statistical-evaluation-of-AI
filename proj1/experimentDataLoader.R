#Set working directory yourself Session > Set Working Directory > Source Files Location

#Load data
dir <- getwd()
load(file.path(dir,"armdata.RData"))

#Extracting data from experiment x
x <- 8
#Using [[]] for indexing nested list
expData <- armdata[[x]]

save(expData, file=sprintf("exp%dArmData.rds", x))


